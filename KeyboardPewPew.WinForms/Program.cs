﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;

namespace KeyboardPewPew.WinForms
{
	static class Program
	{

		static Random Rand = new Random();
		static int KeyCount = 0;

		static string SoundDirectory = System.Configuration.ConfigurationManager.AppSettings["sound.directory"];
		static string[] Sounds = Directory.GetFiles(SoundDirectory, "*.*");
		static int KeyCountTrigger = int.Parse(System.Configuration.ConfigurationManager.AppSettings["key.count"]);

		private const int WH_KEYBOARD_LL = 13;
		private const int WM_KEYDOWN = 0x0100;
		private static LowLevelKeyboardProc _proc = HookCallback;
		private static IntPtr _hookID = IntPtr.Zero;


		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool UnhookWindowsHookEx(IntPtr hhk);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr GetModuleHandle(string lpModuleName);

		private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

		[STAThread]
		static void Main(string[] args)
		{
			_hookID = SetHook(_proc);
			
			Application.Run();

			UnhookWindowsHookEx(_hookID);
		}

		private static IntPtr SetHook(LowLevelKeyboardProc proc)
		{
			using (Process curProcess = Process.GetCurrentProcess())
			using (ProcessModule curModule = curProcess.MainModule)
			{
				return SetWindowsHookEx(WH_KEYBOARD_LL, proc, GetModuleHandle(curModule.ModuleName), 0);
			}
		}

		private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
		{
			if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)
			{
				int vkCode = Marshal.ReadInt32(lParam);

				new System.Threading.Thread(() => PlaySound()).Start();
			}
			return CallNextHookEx(_hookID, nCode, wParam, lParam);
		}

		private static void PlaySound() 
		{
			if (++KeyCount == KeyCountTrigger)
			{
				try
				{
					var player = new System.Media.SoundPlayer(Sounds[Rand.Next(0, Sounds.Length)]);
					player.Play();
				}
				catch { }

				KeyCount = 0;
			}
		}
	}
}
